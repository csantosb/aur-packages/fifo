-- * header
-------------------------------------------------------------------------------
-- Title      : Dual-port block RAM
-- Project    :
-------------------------------------------------------------------------------
-- File       : dual_port_ram.vhd
-- Author     : Jose Correcher  <jcorrecher@gmail.com>
-- Company    :
-- Created    : 2020-05-25
-- Last update: 2020-07-29
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module contains a behavioral dual port.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-05-25  1.0      jcorrecher	Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity dual_port_ram is

  port (
    clka     : in  std_logic;
    clkb     : in  std_logic;
    ic_wena  : in  std_logic;
    ic_wenb  : in  std_logic;
    ic_rena  : in  std_logic;
    ic_renb  : in  std_logic;
    ic_addra : in  unsigned;
    ic_addrb : in  unsigned;
    id_porta : in  unsigned;
    id_portb : in  unsigned;
    od_porta : out unsigned;
    od_portb : out unsigned);

end entity dual_port_ram;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of dual_port_ram is

-- ** type declaration

  type ram_type is array (0 to (2 ** ic_addra'length) - 1) of unsigned(id_porta'range);

  shared variable ram : ram_type := (others => (others => '0'));

  signal b0_data_r : unsigned(od_porta'range) := (others => '0');
  signal b1_data_r : unsigned(od_portb'range) := (others => '0');

begin

-------------------------------------------------------------------------------
-- * B0 : port a
-------------------------------------------------------------------------------

  process (clka) is
  begin
    if rising_edge(clka) then
      if (ic_wena = '1') then
        ram(to_integer(ic_addra)) := id_porta;
      end if;
      if (ic_rena = '1') then
        b0_data_r <= ram(to_integer(ic_addra));
      end if;
    end if;
  end process;

-------------------------------------------------------------------------------
-- * B1 : port b
-------------------------------------------------------------------------------

  process (clkb) is
  begin
    if rising_edge(clkb) then
      if (ic_wenb = '1') then
        ram(to_integer(ic_addrb)) := id_portb;
      end if;
      if (ic_renb = '1') then
        b1_data_r <= ram(to_integer(ic_addrb));
      end if;
    end if;
  end process;

  od_porta <= b0_data_r;
  od_portb <= b1_data_r;

end architecture rtl;
